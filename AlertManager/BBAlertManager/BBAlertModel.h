//
//  BBAlertModel.h
//  AlertManager
//
//  Created by aron on 2020/8/2.
//  Copyright © 2020 aron. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BBAlertModel : NSObject

/// 级别
@property (nonatomic, assign) NSInteger level;

/// 执行Block
@property (nonatomic, copy) void (^executeBlock)(BBAlertModel *alertModel);

/// 关闭Block
@property (nonatomic, copy) void (^closeBlock)(void);

@end

NS_ASSUME_NONNULL_END
