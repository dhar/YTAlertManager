//
//  BBAlertManger.h
//  AlertManager
//
//  Created by aron on 2020/8/2.
//  Copyright © 2020 aron. All rights reserved.
//

#import "BBAlertModel.h"
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BBAlertManger : NSObject

+ (instancetype)sharedManager;

- (void)executeAlertWithModel:(BBAlertModel *)alertModel;

@end

NS_ASSUME_NONNULL_END
