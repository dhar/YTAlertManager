//
//  ViewController.m
//  AlertManager
//
//  Created by aron on 2020/8/2.
//  Copyright © 2020 aron. All rights reserved.
//

#import "ViewController.h"
#import "BBAlertManger.h"
#import "BBAlertModel.h"

@interface ViewController ()
@property (nonatomic, assign) NSInteger count;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (int i = 0; i < 5; i++) {
        [self addAlertWithCount:i];
    }
}

- (void)addAlertWithCount:(NSInteger)count {
    BBAlertModel *alertModel = [BBAlertModel new];
    alertModel.level = count;
    alertModel.executeBlock = ^(BBAlertModel *_Nonnull alertModel) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"第%d个", (int)count] message:[NSString stringWithFormat:@"第%d个弹窗", (int)count] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消"
                                                  style:UIAlertActionStyleCancel
                                                handler:^(UIAlertAction *_Nonnull action) {
                                                    alertModel.closeBlock();
                                                }]];
        [self presentViewController:alert animated:YES completion:nil];
    };
    [[BBAlertManger sharedManager] executeAlertWithModel:alertModel];
}


@end
